package stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class openGooleDefinitions {
	
	WebDriver driver;
	@Given("^uzer is entering google\\.com$")
	public void uzer_is_entering_google_com() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\john\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver =new ChromeDriver();
		driver.get("http://www.google.com");
	}

	@When("^user is typing the search tern \"([^\"]*)\"$")
	public void user_is_typing_the_search_tern(String searchTerm) throws Throwable {
	    driver.findElement(By.name("q")).sendKeys(searchTerm);
	}

	@When("^enters the return key$")
	public void enters_the_return_key() throws Throwable {
		 driver.findElement(By.name("q")).sendKeys(Keys.RETURN);
	}

	@Then("^the user should see the search results$")
	public void the_user_should_see_the_search_results() throws Throwable {
	   boolean status= driver.findElement(By.partialLinkText("John")).isDisplayed();
	   if(status) {
		   System.out.println("Results successfully Displayed");
	   }
	   driver.close();
	}
}
